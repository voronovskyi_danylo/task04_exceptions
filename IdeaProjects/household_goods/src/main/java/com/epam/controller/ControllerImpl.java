package com.epam.controller;

import com.epam.model.*;

import java.util.List;

public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    public List<DishDetergent> getDishDetergentList() {
        return model.getDishDetergentList();
    }

    public List<DishDetergent> dishBucketOperation() {
        return model.dishBucketOperation();
    }

    public List<FloorDetergent> getFloorDetergentList() {
        return model.getFloorDetergentList();
    }

    public List<FloorDetergent> floorBucketOperation() {
        return model.floorBucketOperation();
    }

    public List<FurnitureDetergent> getFurnitureDetergentList() {
        return model.getFurnitureDetergentList();
    }

    public List<FurnitureDetergent> furnitureBucketOperation() {
        return model.furnitureBucketOperation();
    }


}

