package com.epam;

import com.epam.exception.NotFoundException;

import java.util.ArrayList;
import java.util.List;

public class Application implements AutoCloseable {
    public void close() throws Exception {
        System.out.println("Problems..");
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        try {
            list.get(12);
        }catch (ArrayIndexOutOfBoundsException a){
            throw new NotFoundException("Element with such index not found");
        }
    }
}
